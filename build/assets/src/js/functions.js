jQuery(document).ready(function($) {

  // clear and restore form input values
  var el = $('input[type=text], input[type=email], textarea, input.header-search');
  el.focus(function(e) {
    if (e.target.value === e.target.defaultValue)
      e.target.value = '';
  });
  el.blur(function(e) {
    if (e.target.value === '')
      e.target.value = e.target.defaultValue;
  });

  var dt = new Date();
  function addZero(i) {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  }
  var time = addZero(dt.getHours()) + ":" + addZero(dt.getMinutes());

  if (Cookies('latitude') && Cookies('longitude')) {
    $('.js-geolocation').hide();
  } else {
    if ("geolocation" in navigator) {
      $('.js-geolocation').show(); 
    } else {
      $('.js-geolocation').hide();
    }
  }

  if (Cookies('latitude') && Cookies('longitude')) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var latitude = Cookies.get('latitude');
      var longitude = Cookies.get('longitude');
      loadWeather(latitude+','+longitude);
    });
  } else {
    $('.js-geolocation').on('click', function() {
      navigator.geolocation.getCurrentPosition(function(position) {
        loadWeather(position.coords.latitude+','+position.coords.longitude); //load weather using your lat/lng coordinates
        Cookies.set('latitude', position.coords.latitude);
        Cookies.set('longitude', position.coords.longitude);
      });
      $('.js-geolocation').hide();
    });
  }

  $(document).ready(function() {
    loadWeather('Springfield, IL'); //@params location, woeid
  });

  function loadWeather(location, woeid) {
    $.simpleWeather({
      location: location,
      woeid: woeid,
      unit: 'f',
      success: function(weather) {
        html = '<h3 class="today text-center"><i class="icon-'+weather.code+'"></i><span>'+weather.temp+'&deg;'+weather.units.temp+'</span></h3>';
        html += '<div class="location text-center">'+weather.city+', '+weather.region+' | '+weather.currently+' | '+time+'</div>';

        html += '<div class="forecast">';
        for(var i=0;i<weather.forecast.length;i++) {
          html += '<div class="forecast-day"><p><i class="icon-'+weather.code+' small"></i><br/>'+weather.forecast[i].day+': '+weather.forecast[i].high+'&deg;'+weather.units.temp+'</p></div>';
        }
        html += '</div>';
        
        $(".weather-info").html(html);
      },
      error: function(error) {
        $(".weather-info").html('<p>'+error+'</p>');
      }
    });
  }

  // quote rss
  // function parseRSS(url, container) {
  //   $.ajax({
  //     url: document.location.protocol + '//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=10&callback=?&q=' + encodeURIComponent(url),
  //     dataType: 'json',
  //     success: function(data) {
  //       $.each(data.responseData.feed.entries, function(key, value){
  //         if (key == 1) {
  //           return false;
  //         }
  //         var entry = '<blockquote cite="'+value.link+'"><p>'+value.content+'</p><footer>'+value.title+'</footer></blockquote>';
  //         $(container).append(entry);
  //       });
  //     }
  //   });
  // }

  // $(function(){
  //   // running custom RSS functions
  //   parseRSS('http://feeds.feedburner.com/brainyquote/QUOTEBR', '.quote');
  // });

  // background rss
  // function backgroundRSS(url) {
  //   $.ajax({
  //     url: document.location.protocol + '//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=10&callback=?&q=' + encodeURIComponent(url),
  //     dataType: 'json',
  //     async: 'false',
  //     success: function(data) {
  //       $.each(data.responseData.feed.entries, function(key, value){
  //         if (key == 1) {
  //           return false;
  //         }
  //         var entry = value.content;
  //         var regex = /<img.+?src=[\"'](.+?)[\"'].*?>/;
  //         background = regex.exec(entry)[1];
  //         // var background = entry.getAttribute("src");
  //         // console.log(background);
  //         $('body').css('background-image', 'url('+background+')');
  //         // $(container).append(entry);
  //       });
  //     }
  //   });
  // }

  // $(function(){
  //   backgroundRSS('http://imgur.com/r/earthporn/rss');
  // });

  // var images = [
  //   '/assets/dist/images/backgrounds/.jpg',
  //   '/assets/dist/images/backgrounds/.jpg',
  //   '/assets/dist/images/backgrounds/.jpg',
  //   '/assets/dist/images/backgrounds/.jpg',
  //   '/assets/dist/images/backgrounds/.jpg',
  //   '/assets/dist/images/backgrounds/.jpg',
  //   '/assets/dist/images/backgrounds/.jpg',
  //   '/assets/dist/images/backgrounds/.jpg',
  //   '/assets/dist/images/backgrounds/.jpg',
  //   '/assets/dist/images/backgrounds/.jpg',
  //   '/assets/dist/images/backgrounds/.jpg'
  // ];
  // $('.body').css({'background-image': 'url('+images[Math.floor(Math.random() * images.length)]+')'});

  // random backgrounds
  $.ajax({
    url: '/assets/dist/js/backgrounds.json',
    type: 'get',
    dataType: 'json',
    async: false,
    success: function(data) {
      var background = data[Math.floor(Math.random()*data.length)];
      $('body').css('background-image', 'url('+background+')');
    } 
  });

  // random quotes
  $.ajax({
    url: '/assets/dist/js/quotes.json',
    type: 'get',
    dataType: 'json',
    async: false,
    success: function(data) {
      var quote = data[Math.floor(Math.random() * data.length)];
      $('.quote').append('<blockquote><p>'+quote[0]+'</p><footer>'+quote[1]+'</footer></blockquote>');
    } 
  });

});
