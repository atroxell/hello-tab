// VARIABLES
var gulp = require('gulp');
var gutil = require('gulp-util');

var color = require('chalk');
var scsslint = require('gulp-scss-lint');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
//var minifyCSS = require('gulp-minify-css');
var nano = require('gulp-cssnano');

var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

var dirSync = require('gulp-directory-sync');

var sourcemaps = require('gulp-sourcemaps');
var notify = require('gulp-notify');
var rename = require('gulp-rename');
var browserSync = require('browser-sync');

var gulpif = require('gulp-if');
var argv = require('yargs').argv;
var prod = !!(argv.prod);

// BASE PATHS
var basePaths = {
  src: 'assets/src/',
  dest: 'assets/dist/'
};

// ASSET FOLDER PATHS
var paths = {
  images: {
    src: basePaths.src + 'images/',
    dest: basePaths.dest + 'images/',
  },
  scripts: {
    src: basePaths.src + 'js/',
    dest: basePaths.dest + 'js/'
  },
  styles: {
    src: basePaths.src + 'scss/',
    dest: basePaths.dest + 'css/'
  }
};

// ASSET FILES
var files = {
  // IMAGES
  images: paths.images.src + '**/*.{png,gif,jpg,svg}',
  // SASS
  styles: [paths.styles.src + '**/*.scss', '!' + paths.styles.src + 'foundation{,/**}', '!' + paths.styles.src + 'vendor{,/**}'],
  // JS
  scripts: [paths.scripts.src + 'vendor/jquery-1.11.1.min.js', paths.scripts.src + 'vendor/modernizr.min.js', paths.scripts.src + 'foundation/foundation.js', paths.scripts.src + 'vendor/js-cookie/js.cookie.js', paths.scripts.src + 'vendor/simpleWeather/jquery.simpleWeather.js', paths.scripts.src + 'functions.js']
};

// BROWSERSYNC
gulp.task('browser-sync', function() {
  var browserSyncFiles = ['*.{html,php,aspx,ascx,asp}', 'assets/dist/css/*.css', 'assets/dist/js/*.js', 'assets/dist/images/**/*.{png,gif,jpg,svg}'];
  browserSync.init(browserSyncFiles, {
    server: {
      baseDir: "./"
    }
  });
});

// IMAGE OPTIMIZATION
gulp.task('images', function () {
  gulp.src(paths.images.src)
  .pipe(dirSync(paths.images.src, paths.images.dest, { printSummary: true } ))
  .on('error', gutil.log)
  .pipe(notify({message:'Images copied successfully', onLast: true}));
});

// SCSS LINT REPORTER
var reporter  = function(file) {
  if (file.scsslint.success) {
    return;
  } else {
    gutil.beep();
  }
  file.scsslint.issues.forEach(function (issue) {
    gutil.log(
      color.cyan(file.relative) + ':' + color.magenta(issue.line),
      color.yellow('[' + (issue.severity === 'warning' ? 'W' : 'E') + ']'),
      color.green(issue.linter + ':'),
      issue.reason
    );
  });
}

// SCSS LINT
gulp.task('scsslint', function() {
  gulp.src([paths.styles.src + 'components/*.scss', paths.styles.src + 'layout/*.scss', '!' + paths.styles.src + 'components/_dnn.scss', '!' + paths.styles.src + 'components/_forms.scss'])
  .pipe(scsslint({
    'config': '.scss-lint.yml',
    customReport: reporter
  }));
});

// STYLES
gulp.task('styles', ['scsslint'], function() {
  gulp.src(files.styles)
  .pipe(gulpif(!prod, sourcemaps.init()))
  .pipe(sass().on('error', sass.logError))
  .on('error', notify.onError({ message: 'Styles failed'}))
  .pipe(gulpif(prod, autoprefixer('last 2 versions')))
  .pipe(gulpif(prod, nano()))
  .pipe(gulpif(!prod, sourcemaps.write('.')))
  .pipe(gulp.dest(paths.styles.dest))
  .pipe(notify({message:'Styles compiled successfully', onLast: true}));
});

// JS LINT
gulp.task('jslint', function() {
  gulp.src(paths.scripts.src + 'functions.js')
  .pipe(jshint())
  .pipe(jshint.reporter('jshint-stylish'))
  .pipe(jshint.reporter('fail'))
  .on('error', notify.onError({ message: 'JS lint failed'}));
});

// SCRIPTS
gulp.task('scripts', ['jslint'], function() {
  return gulp.src(files.scripts)
  .pipe(gulpif(!prod, sourcemaps.init()))
  .pipe(concat('main.js'))
  .pipe(gulpif(prod, uglify().on('error', notify.onError({ message: 'Scripts failed'}))))
  .pipe(gulpif(!prod, sourcemaps.write('.')))
  .pipe(gulp.dest(paths.scripts.dest))
  .pipe(notify({message: 'Scripts compiled successfully', onLast:true}));
});

// DEFAULT WATCH TASK
gulp.task('default', ['styles', 'scripts', 'images'], function () {
  gulp.watch(paths.styles.src + '**/*.scss', ['styles']);
  gulp.watch(paths.scripts.src + '**/*.js', ['scripts']);
  gulp.watch(paths.images.src + '**/*.{png,gif,jpg,svg}', ['images']);
});

// BROWSERSYNC GULP TASK
gulp.task('sync', ['styles', 'scripts', 'images', 'browser-sync'], function () {
  gulp.watch(paths.styles.src + '**/*.scss', ['styles']);
  gulp.watch(paths.scripts.src + '**/*.js', ['scripts']);
  gulp.watch(paths.images.src + '**/*.{png,gif,jpg,svg}', ['images']);
});
